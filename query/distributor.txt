CREATE TABLE distributor (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    addresstype VARCHAR(50) NOT NULL,
    address_id INT,
    FOREIGN KEY (address_id) REFERENCES address(id)
);